package goarea

import "math"

// Pi default value
const Pi = 3.1416

// Circle is responsible to calculate the circunference area
func Circle(r float64) float64 {
	return math.Pow(r, 2) * Pi
}

// Rectangule is responsible to calculate the squad area
func Rectangule(base, height float64) float64 {
	return base * height
}

// This function is not visible because of '_' you could also use 'triangleEq'
func _TriangleEq(base, height float64) float64 {
	return (base * height) / 2
}
